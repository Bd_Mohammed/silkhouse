export const SearchSVG = () => (
  <svg
    width="17"
    height="16"
    viewBox="0 0 17 16"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <path
      d="M7.83398 13.333C11.1477 13.333 13.834 10.6467 13.834 7.33301C13.834 4.0193 11.1477 1.33301 7.83398 1.33301C4.52028 1.33301 1.83398 4.0193 1.83398 7.33301C1.83398 10.6467 4.52028 13.333 7.83398 13.333Z"
      stroke="#C5C5C5"
      stroke-width="1.5"
      stroke-linecap="round"
      stroke-linejoin="round"
    />
    <path
      d="M13.1196 13.7929C13.473 14.8595 14.2796 14.9662 14.8996 14.0329C15.4663 13.1795 15.093 12.4795 14.0663 12.4795C13.3063 12.4729 12.8796 13.0662 13.1196 13.7929Z"
      stroke="#C5C5C5"
      stroke-width="1.5"
      stroke-linecap="round"
      stroke-linejoin="round"
    />
  </svg>
);
