import * as React from 'react';
import { Navbar } from '../../component/Navbar';
import { Footer } from '../../component/Footer';
import './landing.css';
import welcomeResort from '../../assets/welcome-resort.png';

export const LandingPage = () => {
  return <>
    <Navbar />
    <div className='container' >
      <div className='welcome-resort'>
        <img className="w-fill" src={welcomeResort} alt='welcome-resort' />
      </div>
    </div>
    <Footer />
  </>;
};
