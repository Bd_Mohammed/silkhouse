import { InstagramSVG } from '../../assets/instagram';
import { LinkdinSVG } from '../../assets/linkdin';
import { LogoSVG } from '../../assets/logo';
import { TwitterSVG } from '../../assets/twitter';

export const Footer = () => {
  return (
    <footer className="flex-center bg-primary-dark-green w-full h-320">
      <div className="w-427 h-164">
        <div className="text-center">
          <a href="#">
            <LogoSVG color={'white'} />
          </a>
        </div>
        <div className="text-center white">Connect with Us and Explore</div>
        <div className="text-center mt-32">
          <TwitterSVG color={'white'} />
          <InstagramSVG color={'white'} className={'mx-32'}/>
          <LinkdinSVG color={'white'} />
        </div>
      </div>
    </footer>
  );
};
