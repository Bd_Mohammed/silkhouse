import * as React from 'react';
import { BellSVG } from '../../assets/bell';
import { UserSVG } from '../../assets/user';
import { DropDownArrowSVG } from '../../assets/dropDownArrow';
import { LocationSVG } from '../../assets/location';
import { SearchSVG } from '../../assets/serach';
import { LogoSVG } from '../../assets/logo';

export const Navbar = () => {
  return (
    <nav className="navbar d-flex justify-content-between align-center px-40 py-20">
      <a href="#" className=''>
        <LogoSVG />
      </a>

      <div className='flex-center h-48 w-425 border-gray-1 r-8'>
        <input
          type="text"
          className="h-14 w-367 border-none"
          placeholder="Search restaurant and cuisines..."
        />
        <SearchSVG />
      </div>

      <div className="w-331 h-48 d-flex justify-content-between">
        <div className="d-flex justify-content-between align-center w-211 h-48 r-8 border-gray-1 px-12 py-9">
          <div className="d-flex h-30">
            <span className="py-5 pr-8">
              <LocationSVG />
            </span>
            <div>
              <div className="fs-8 lh-16">Your Location</div>
              <div className="fs-12 lh-14">Varanasi</div>
            </div>
          </div>
          <DropDownArrowSVG />
        </div>
        <button
          type="button"
          className="flex-center w-48 h-48 r-8 p-14 bg-dark-green-800 border-transparent"
        >
          <div>
            <BellSVG />
          </div>
        </button>
        <button
          type="button"
          className="flex-center w-48 h-48 r-8 p-14 bg-dark-green-800 border-transparent"
        >
          <div>
            <UserSVG />
          </div>
        </button>
      </div>
    </nav>
  );
};

// /* logo */

// /* Auto layout */
// display: flex;
// flex-direction: row;
// justify-content: center;
// align-items: center;
// padding: 0px;
// gap: 10px;

// margin: 0 auto;
// width: 99px;
// height: 48px;

// /* Inside auto layout */
// flex: none;
// order: 0;
// align-self: stretch;
// flex-grow: 0;
